<?php

namespace Drupal\ar_glue\Plugin\Block;

use Drupal\ar_glue\InitiativeManager;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'InitiativeCloseButtonBlock' block.
 *
 * @Block(
 *  id = "initiative_close_button_block",
 *  admin_label = @Translation("Initiative close button block"),
 * )
 */
class InitiativeCloseButtonBlock extends BlockBase {


  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $node = \Drupal::routeMatch()->getParameter('node');

    if (!$node || $node->bundle() != 'initiative') {
      return FALSE;
    }

    $initiativeManager = new InitiativeManager($node);
    $report = $initiativeManager->getReport();

    if (!empty($report)) {
      $build['initiative_close_button_block']['#markup'] = '
        <a href="' . ar_glue_node_url($report->id()) . '" class="hamburger hamburger--slider is-active">
            <span class="hamburger-prefix">Home</span>
            <span class="hamburger-box">
                <span class="hamburger-inner"></span>
            </span>
        </a>
    ';
    }

    $build['#cache']['max-age'] = 0;
    return $build;
  }

}
