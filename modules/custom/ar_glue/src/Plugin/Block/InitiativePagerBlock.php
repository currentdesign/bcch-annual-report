<?php

namespace Drupal\ar_glue\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'InitiativePagerBlock' block.
 *
 * @Block(
 *  id = "initiative_pager_block",
 *  admin_label = @Translation("Initiative pager block"),
 * )
 */
class InitiativePagerBlock extends BlockBase {


  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $node = \Drupal::routeMatch()->getParameter('node');

    if (!$node || $node->bundle() != 'initiative') {
      return FALSE;
    }

    $build['initiative_pager_block'] = [
      '#theme' => 'ar_glue_initiative_pager',
      '#current_initiative' => $node
    ];

    $build['#cache']['max-age'] = 0;
    return $build;
  }

}
