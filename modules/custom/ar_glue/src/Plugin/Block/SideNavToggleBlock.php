<?php

namespace Drupal\ar_glue\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'SideNavToggleBlock' block.
 *
 * @Block(
 *  id = "side_nav_toggle_block",
 *  admin_label = @Translation("Side nav toggle block"),
 * )
 */
class SideNavToggleBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['side_nav_toggle_block']['#markup'] = '
      <div class="sidenav-toggle">
        <span class="sidenav-toggle__labels">
            <span>Menu</span>
            <span class="hidden">Close</span>
        </span>
        <a class="hamburger hamburger--spin">
            <span class="hamburger-box">
                <span class="hamburger-inner"></span>
            </span>
        </a>
      </div>';

    $build['#cache']['max-age'] = 0;
    return $build;
  }

}
