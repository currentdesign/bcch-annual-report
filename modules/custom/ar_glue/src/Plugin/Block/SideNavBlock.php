<?php

namespace Drupal\ar_glue\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'SideNavBlock' block.
 *
 * @Block(
 *  id = "sidenav_block",
 *  admin_label = @Translation("Sidenav block"),
 * )
 */
class SideNavBlock extends BlockBase {


  /**
   * {@inheritdoc}
   */
  public function build() {

    $node = \Drupal::routeMatch()->getParameter('node');
    $build = [];

    if ($node && $node->bundle() == 'report') {

      $links = [
        ['title' => 'Home', 'href' => '#home', 'active'=> 1]
      ];

      foreach ($node->get('field_content')->referencedEntities() as $entity) {

        switch ($entity->bundle()) {

          case 'intro_section':
            $links[] = array('title' => 'Introduction', 'href' => '#intro_section');
            break;

          /*
           case 'section_with_cta':
            break;
            */

          case 'initiatives':
            $children = [];
            if ($entity->hasField('field_initi')) {
              foreach ($entity->get('field_initi')->referencedEntities() as $initiative) {
                $children[] = array('title' => $initiative->getTitle(), 'href' => ar_glue_node_url($initiative->id()));
              }
            }
            $links[] = array(
              'title' => $entity->get('field_heading')->value,
              'href' => '#' . $entity->bundle(),
              'children' => $children
            );
            break;

          default:
            if ($entity->hasField('field_heading')) {
              $links[] = array('title' => $entity->get('field_heading')->value, 'href' => '#' . $entity->bundle());
            }
            break;
        }

      }

      /*if (!empty($node->get('field_menu'))) {

        $menu_tree = \Drupal::menuTree();

        // Build the typical default set of menu tree parameters.
        $parameters = $menu_tree->getCurrentRouteMenuTreeParameters($node->get('field_menu')->target_id);

        // Load the tree based on this set of parameters.
        $tree = $menu_tree->load($node->get('field_menu')->target_id, $parameters);

        // Transform the tree using the manipulators you want.
        $manipulators = array(
          // Only show links that are accessible for the current user.
          array('callable' => 'menu.default_tree_manipulators:checkAccess'),
          // Use the default sorting of menu links.
          array('callable' => 'menu.default_tree_manipulators:generateIndexAndSort'),
        );
        $tree = $menu_tree->transform($tree, $manipulators);

        $menu = $menu_tree->build($tree);

        $menu['#prefix'] = '<div id="sidenav">';
        $menu['#suffix'] = '</div>';

        $build['sidenav'] = $menu;
      }*/

      if (!empty($links)) {
        $build = [
          '#theme' => 'ar_glue_sidebar_nav',
          '#links' => $links
        ];
      }

    }

    $build['#cache']['max-age'] = 0;
    return $build;
  }



}
