<?php

namespace Drupal\ar_glue\Plugin\Field\FieldFormatter;

use Drupal\link\LinkItemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;

/**
 * Plugin implementation of the 'link_with_class' formatter.
 *
 * @FieldFormatter(
 *   id = "link_with_class",
 *   label = @Translation("Link with class"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class LinkWithClass extends LinkFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return parent::defaultSettings() +
    ['class' => ''];

  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['class'] = array(
      '#type' => 'textfield',
      '#title' => t('Class on Link'),
      '#default_value' => $this->getSetting('class'),
    );

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {

    $summary = parent::settingsSummary();

    $settings = $this->getSettings();

    if (!empty($settings['class'])) {
      $summary[] = t('Class(es) on button = "@classes"', array('@classes' => $settings['class']));
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildUrl(LinkItemInterface $item) {
    $url = parent::buildUrl($item);

    $settings = $this->getSettings();

    if (!empty($settings['class'])) {
      $options = $url->getOptions();
      $options['attributes']['class'] = $settings['class'];
      $url->setOptions($options);
    }

    return $url;
  }

}
