<?php

namespace Drupal\ar_glue;


/**
 * Class InitiativeManager.
 *
 * @package Drupal\ar_glue
 */
class InitiativeManager {

  /**
   * Constructor.
   */
  public function __construct($initiative) {
    $this->initiative = $initiative;
    $this->initiatives = [];
  }

  private function getNextOrPrevInitiative($prev = FALSE) {
    $next = FALSE;
    $initiative_ids = $this->getReportInitiativeIds();
    if ($prev) {
      $initiative_ids = array_reverse($initiative_ids);
    }
    foreach ($initiative_ids as $initiative_id) {
      if ($next) {
        return node_load($initiative_id);
      }
      if ($this->initiative->id() == $initiative_id) {
        $next = TRUE;
      }
    }
    return FALSE;
  }

  public function getNextInitiative() {
    return $this->getNextOrPrevInitiative();
  }

  public function getPreviousInitiative() {
    return $this->getNextOrPrevInitiative(TRUE);
  }

  public function getReportInitiativeIds() {
    if (empty($this->initiatives)) {
      $report_id = $this->getReportId();
      $query = db_select('paragraph__field_initi', 'i');
      $query->addField('i', 'field_initi_target_id', 'id');
      $query->leftJoin('paragraphs_item_field_data', 'p', "i.entity_id = p.id AND i.deleted = 0 AND i.bundle = 'initiatives'");
      $query
        ->condition('p.parent_type', 'node')
        ->condition('p.parent_id', $report_id)
        ->condition('p.parent_field_name', 'field_content');
      $query->orderBy('i.delta', 'ASC');
      $this->initiatives = $query->execute()->fetchCol();
    }
    return $this->initiatives;
  }

  public function getReport() {
    $report_id = $this->getReportId();
    return $report_id ? node_load($report_id) : NULL;
  }

  public function getReportId() {
    $report_ids = $this->getReportIds();
    return !empty($report_ids) ? reset($report_ids) : NULL;
  }

  public function getReportIds() {
    $query = db_select('paragraph__field_initi', 'i');
    $query->addField('p', 'parent_id');
    $query->leftJoin('paragraphs_item_field_data', 'p', "i.entity_id = p.id AND i.deleted = 0 AND i.bundle = 'initiatives'");
    $query->condition('i.field_initi_target_id', $this->initiative->id())
      ->condition('p.parent_type', 'node')
      ->condition('p.parent_field_name', 'field_content');
    return $query->execute()->fetchCol();
  }

}
