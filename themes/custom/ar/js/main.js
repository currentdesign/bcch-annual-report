(function ($) {

  // Global state holder for auto-scroll.
  var arAutoScroll = false;

  var spinner = $('.spinner__container');

  Drupal.behaviors.ar_main = {
    attach: function(context) {

      var toggle = $('#block-sidenavtoggleblock .sidenav-toggle'),
          sidenavBlock = $('#block-sidenavblock'),
          anchor = $('.paragraph--type--intro-section__anchor'),
          initiativeMenu = $('.initiative-menu'),
          intitiativeScrollArea = $('.initiative-scrollarea'),
          body = $('body');

      // React on #clicks and scroll to a target.
      anchor.arScrollToTarget(body, -95);
      sidenavBlock.arScrollToTarget(body, -95);
      initiativeMenu.arScrollToTarget(intitiativeScrollArea);

      // Show active link on scroll.
      sidenavBlock.arUpdateActiveLinksOnScroll();
      initiativeMenu.arUpdateActiveLinksOnScroll(intitiativeScrollArea);

      // Toggle sidebar.
      toggle.once().arToggleSideNav(sidenavBlock);
    }
  };

  // Drupal.behaviors.ar_featured_equal_heights = {
  //   attach: function (context) {
  //     $('.field--type-entity-reference-revisions').arEqualHeights('.featured-content-item__right');

  //   }
  // };

  Drupal.behaviors.ar_lettering = {
    attach: function(context) {
      // Wraps every word of H1 into spans for per-word styling.
      $('h1').lettering('words');
    }
  };

  Drupal.behaviors.ar_ajax_load = {
    attach: function (context) {

      var initiatives = $('#initiatives');

      // showSingleInitiative();

      $(document).once().on('click', 'a.ajax-load', function(e) {
        e.preventDefault();
        spinner.removeClass('spinner__container--hidden');
        loadInitiative(initiatives, $(this).attr('href'), $(this).hasClass('animate-reverse'));
      });

      $(document).on('click', '.block-initiative-close-button-block a', function(e) {
        if ($('body').hasClass('page-node-type-report')) {
          e.preventDefault();
          closeInitiative();
        }
      });

      $(window).bind('popstate', function(e) {
        var href = window.location.href;
        if (!window.location.hash) {
          if (e.originalEvent.state && e.originalEvent.state.initiative) {
            loadInitiative(initiatives, href, true);
          }
          else {
            window.location.replace(href);
          }
        }
      });
    }
  };

  function loadInitiative(initiatives, url, reverse) {
    var content = initiatives.find('div[data-url="' + url + '"]');

    if (content.length == 0) {
      $.get(url, function (data) {
      }).done(function(data) {
        var content = $(data).filter('#wrapper'),
          cssClasses = content.attr("class");

        content.find('#toolbar-administration').remove();
        content = '<div class="initiatives__item ' + cssClasses +'" data-url="' + url + '">' + content.html() + '</div>';
        if (content) {
          addInitiative(initiatives, content, url, reverse);
        }
      });
    }
    else {
      addInitiative(initiatives, content, url, reverse);
    }
  }

  function addInitiative(initiatives, content, url, reverse) {
    history.pushState({initiative: true}, null, url);

    var scrollDistance = $("body").scrollTop();
    $("body").addClass('pjax-overlay').scrollTop(scrollDistance);
    initiatives.append(content);
    revealInitiative(url, reverse);
    initiatives.addClass('is-active');
  }

  function showSingleInitiative() {
    var left_column = $('.initiative-left-column'),
      right_column = $('.initiative-right-column');

    if (!isMobile()) {

      TweenMax.to(right_column, 0, {
        x: "0%",
        y: "100%"
      });

      TweenMax.to(left_column, 0, {
        x: "100%",
        y: "0%"
      });
      left_column.hide();

      return TweenMax.to(right_column, .5, {
        y: "0%",
        // ease: $.bez([0.250, 0.440, 0.440, 0.930]),
        ease: Power1.easeInOut,
        //force3D: true,
        onComplete: function () {
          left_column.show();
          return TweenMax.to(left_column, .7, {
            css: {
              x: "0%"
            },
            // ease: $.bez([0.250, 0.440, 0.440, 0.930])
            ease: Power1.easeInOut,
            //force3D: true
          });
        }
      });

    }
    else {
      // Mobile resolution
      TweenMax.to(right_column, 0, {
        x: "0%",
        y: "650px"
      });
      left_column.show();
      TweenMax.to(left_column, 0, {
        x: "0%",
        y: "100%"
      });
      // $('#js-topic-over-filter').addClass('active');
      TweenMax.to(right_column, .5, {
        y: "0%"
      });
      return TweenMax.to(left_column, .5, {
        y: "0%",
        // ease: $.bez([0.250, 0.440, 0.440, 0.930])
        ease: Power1.easeInOut,
        //force3D: true
      });

    }
  }

  function closeInitiative() {
    history.pushState(null, null, '/');
    $("body").removeClass('pjax-overlay');

    var initiatives = $("#initiatives"),
        initiative = initiatives.find('.initiatives__item.is-active'),
        inactive_initiatives = initiatives.find('.initiatives__item:not(.is-active)'),
        left_column = initiative.find('.initiative-left-column'),
        right_column = initiative.find('.initiative-right-column');
        inactive_initiatives.hide();

    if (!isMobile()) {
      return TweenMax.to(left_column, .5, {
        css: {
          x: "100%"
        },
        // ease: $.bez([0.250, 0.440, 0.440, 0.930]),
        ease: Power1.easeInOut,
        //force3D: true,
        onComplete: function () {
          left_column.hide();
          TweenMax.to(right_column, .5, {
            css: {
              y: "100%"
            },
            // ease: $.bez([0.250, 0.440, 0.440, 0.930]),
            ease: Power1.easeInOut,
            //force3D: true,
            onComplete: function () {
              $('.initiatives__item').removeClass('is-active');
              initiatives.removeClass('is-active');
              inactive_initiatives.show();
            }
          });
        }
      });
    }
    else {
      // Mobile resolution.
      TweenMax.to(right_column, .5, {
        delay: .3,
        css: {
          x: "100%"
        }
      });
      return TweenMax.to(left_column, .5, {
        delay: .3,
        css: {
          x: "100%"
        },
        // ease: $.bez([0.250, 0.440, 0.440, 0.930]),
        ease: Power1.easeInOut,
        //force3D: true,
        onComplete: function() {
            $('.initiatives__item').removeClass('is-active');
            initiatives.removeClass('is-active');
            inactive_initiatives.show();
          }
      });
    }


  }

  function revealInitiative(url, reverse) {

        var initiatives = $("#initiatives"),
          initiatives_overlay = initiatives.find('.initiatives-overlay'),
          new_initiative = initiatives.find('div[data-url="' + url + '"]'),
          new_initiative_menu = new_initiative.find('.initiative-menu'),
          new_initiative_scroll_area = new_initiative.find('.initiative-scrollarea'),
          left_column = new_initiative.find('.initiative-left-column'),
          right_column = new_initiative.find('.initiative-right-column'),
          old_initiative = initiatives.find(".initiatives__item.is-active"),
          old_left_column = old_initiative.find('.initiative-left-column'),
          old_right_column = old_initiative.find('.initiative-right-column');

        new_initiative_menu.arScrollToTarget(new_initiative.find(new_initiative_scroll_area));
        new_initiative_menu.arUpdateActiveLinksOnScroll(new_initiative_scroll_area);

        initiatives.addClass('is-active');
        initiatives_overlay.addClass('is-active');

        spinner.addClass('spinner__container--hidden');

        if (!isMobile()) {

          if (!reverse) {
            old_initiative.removeClass('is-active');
            TweenMax.to(right_column, 0, {
              x: "0%",
              y: "100%"
            });
            TweenMax.to(left_column, 0, {
              x: "100%",
              y: "0%"
            });
            left_column.hide();
            new_initiative.addClass('is-active');
            setTimeout(function() {
              return TweenMax.to(right_column, .5, {
                y: "0%",
                ease: Power1.easeInOut,
                //force3D: true,
                // ease: $.bez([0.250, 0.440, 0.440, 0.930]),
                onComplete: function () {
                  left_column.show();
                  return TweenMax.to(left_column, .7, {
                    x: "0%",
                    ease: Power1.easeInOut,
                    //force3D: true,
                    // ease: $.bez([0.250, 0.440, 0.440, 0.930]),
                    onComplete: function () {
                      initiatives_overlay.removeClass('is-active');
                    }
                  });
                }
              });
            }, 400);
          }
          else {
            TweenMax.to(right_column, 0, {
              y: "0%"
            });
            left_column.show();
            TweenMax.to(left_column, 0, {
              x: "0%",
              y: "0%"
            });

            return TweenMax.to(old_left_column, .5, {
              x: "100%",
              // ease: $.bez([0.250, 0.440, 0.440, 0.930]),
              ease: Power1.easeInOut,
              //force3D: true,
              onComplete: function () {
                old_left_column.hide();
                TweenMax.to(old_right_column, .5, {
                  y: "100%",
                  // ease: $.bez([0.250, 0.440, 0.440, 0.930]),
                  ease: Power1.easeInOut,
                  //force3D: true,
                  onComplete: function () {
                    old_initiative.removeClass('is-active');
                    new_initiative.addClass('is-active');
                    initiatives_overlay.removeClass('is-active');
                  }
                });
              }
            });
          }
        }
        else {
          // Mobile resolution
          TweenMax.to(right_column, 0, {
            x: "0%",
            y: "650px"
          });
          left_column.show();
          TweenMax.to(left_column, 0, {
            x: "0%",
            y: "100%"
          });
          new_initiative.addClass('active');
          TweenMax.to(right_column, .5, {
            y: "0%",
            // ease: $.bez([0.250, 0.440, 0.440, 0.930]),
            ease: Power1.easeInOut,
            //force3D: true
          });
          return TweenMax.to(left_column, .5, {
            y: "0%",
            // ease: $.bez([0.250, 0.440, 0.440, 0.930]),
            ease: Power1.easeInOut,
            //force3D: true,
            onComplete: function() {
              old_initiative.removeClass('is-active');
              new_initiative.addClass('is-active');
              initiatives_overlay.removeClass('is-active');
            }
          });

        }
  }

  /**
   * Make heights of similar containers equal.
   */
  $.fn.arEqualHeights = function(element_selector) {
    var el = this,
      elements = el.find(element_selector),
      makeHeightsEqual = function () {
        var biggestHeight = 0;
        elements.each(function() {
          if ($(this).height() > biggestHeight) {
            biggestHeight = $(this).outerHeight();
          }
        });
        elements.css('height', biggestHeight)
      };

    if (elements) {
      makeHeightsEqual();
      $(window).on('resize', makeHeightsEqual);
    }

    return this;
  };

  /**
   * Handle # click and scroll to target.
   */
  $.fn.arScrollToTarget = function(element, offset) {

    var el = this,
        links = el.find('a[href*="#"]:not([href="#"])');

    links.on('click', scrollOnClick);

    function scrollOnClick(e) {

      var scrollContainer = element,
          offsetVal = offset || 0;

      if (isMobile()) {
        //scrollContainer = window;
        //offsetVal = -95;
      }
      $(scrollContainer).scrollTo(
        '[data-target=' + e.target.hash.slice(1) + ']',
        {
          axis: 'y',
          duration: 800,
          offset: offsetVal,
          onAfter: function() {
            links.removeClass('is-active');
            $(e.target).addClass('is-active');
          }
        }
      );
    }

    return this;
  };

  /**
   * Update active links while scrolling.
   */
  $.fn.arUpdateActiveLinksOnScroll = function(container) {

    var el = this,
      links = el.find('a'),
      hashedLinks = el.find('a[href*="#"]:not([href="#"])');

    if (!container) {
      container = $(document);
    }

    $(container).on('scroll', function(e) {
      //console.log(hashedLinks);
      // If scroll is automated, we don't need to highlight
      // active links while scrolling.
      if (arAutoScroll) {
        //return false;
      }

      hashedLinks.each(function () {
        var link = $(this);
        var target = container.find('div[data-target="' + link.attr("href").replace('#', '') + '"]');
        if (target && target.position() && target.position().top <= container.scrollTop() && target.position().top + target.height() > container.scrollTop()) {
          links.removeClass('is-active');
          link.addClass('is-active');
        }
      });

    });

    return this;
  };

  /**
   * Show / hide side navigation.
   * @param nav - side nav panel element.
   */
  $.fn.arToggleSideNav = function(nav) {

    var toggle = this,
        toggleLabels = toggle.find('.sidenav-toggle__labels span'),
        hamburger = $('.hamburger'),
        hamburgerToggle = $('.sidenav-toggle');

    hamburgerToggle.on('click', function(e) {
      e.preventDefault();
      toggleLabels.toggleClass('hidden');
      hamburger.toggleClass('is-active');
      nav.toggleClass('is-active');
    });
  };

  /**
   * Determine if current window size is mobile.
   */
  function isMobile() {
    return $(window).innerWidth() < 992;
  }

}( jQuery ));
